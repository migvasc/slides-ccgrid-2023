\RequirePackage{luatex85}
\documentclass[Ligatures=TeX,table,svgnames,usetotalslideindicator,compress,10pt]{beamer}

\usepackage{polyglossia}

\setdefaultlanguage{english}
\disablehyphenation
\usetheme{metropolis}
\usepackage{chemformula}
\setbeamertemplate{section in toc}[sections numbered]

\usepackage[style=verbose          ,backend=biber]{biblatex}
\usepackage{multimedia}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{amssymb}
\usepackage{hyperref}

\addbibresource{ref.bib}
\usepackage{appendixnumberbeamer}

\title{Optimal sizing of a globally distributed low carbon cloud federation}

\subtitle{CCGrid 2023 @ Bangalore}

\author{\textbf{Miguel Vasconcelos}$^{1,2}$, Daniel Cordeiro$^{1}$, Georges Da Costa$^{3}$, Fanny Dufossé$^{2}$, Jean-Marc Nicod$^{4}$ and Veronika Rehn-Sonigo$^{4}$}
\institute
{
  School of Sciences, Arts, and Humanities, University of São Paulo, Brazil$^{1}$\\
  Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, Grenoble, France$^{2}$\\
  IRIT, Université de Toulouse, CNRS UMR 5505, Toulouse, France$^{3}$\\
  FEMTO-ST Institute, Univ. Bourgogne Franche-Comté, CNRS, ENSMM, Besan\c con, France$^{4}$\\
 
}

\date{May 2023}

\begin{document}
\setlength\abovecaptionskip{-3pt}

\renewcommand{\footnotesize}{\scriptsize}


\frame{\titlepage}


\begin{frame}
  \frametitle{Table of Contents}
  \tableofcontents[]
\end{frame}


\section{Introduction}

\begin{frame}{The energy needed for Cloud}
  
  \begin{itemize}
    \item  0.9-1.3\% of global electricity demand in 2021 (IEA)
    \item Increase from 2010 to 2018:  6 \times workload vs 6\% energy \footcite{masanet2020recalibrating}

    \end{itemize}
    
  \begin{center}
    \begin{figure}
      \centering
      \includegraphics[width=.8\textwidth]{images/dc-energy-consumption}
      \caption{Estimates for future energy consumption\footcite{KOOT2021116798}}
    \end{figure}
  \end{center}
  
\end{frame}


\begin{frame}{Reducing the environmental impact of cloud operations}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        
      \item Major cloud providers (Amazon AWS, Apple, Facebook, Google, Microsoft) have projects to integrate renewable energy in their Data Centers (DCs)
      \end{itemize}
      
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \begin{figure}[!h]
          \centering
          \includegraphics[width=\textwidth]{images/google_region_picker.jpeg}
          \caption{Google's region picker tool.}
        \end{figure}
      \end{center}
    \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}{Reducing the environmental impact of cloud operations}
  
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{alertblock}{Intermittent nature of renewables}
        \begin{itemize}
        \item \emph{time of the day}, \emph{weather}, \emph{season
            of the year, and geo location}
        \end{itemize}
      \end{alertblock}
      \begin{itemize}
        
      \item Solar power:
        \begin{itemize}
        \item Costs have fallen by 85\% from 2010 to 2019
        \item Solar irradiation has lower variation than wind speed
        \end{itemize}
        
      \item Lithium-Ion batteries:        
        \begin{itemize}
        \item Efficiency in terms of costs, power and energy density, charge and discharge ratio and self-discharge
          
        \end{itemize}
      \end{itemize}
      
      
    \end{column}
    \qquad
    \begin{column}{0.45\textwidth}
      \begin{center}
        \begin{figure}[!h]
          \centering
          \includegraphics[width=\textwidth]{images/pv_ghi.pdf}
          \caption{Solar Irradiation at different locations in
            2021. Source: NASA's MERRA-2.}
        \end{figure}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}




\begin{frame}{Carbon-Responsive Computing}

  Approaches that are aware of their carbon intensity (carbon aware) and make informed decisions\footfullcite{nafus2021carbonaware}
  
  \begin{itemize}    
    
  \item \alert{Follow-the-renewables}
    
    \begin{itemize}
      
    \item Allocates/Migrates the workload to the DCs that have more renewable/low-carbon power available
    \end{itemize}

  \item \alert{Sizing (or Dimensioning/Capacity planning) the DCs renewable infrastructure}
    \begin{itemize}     
    \item Compute the area of solar panels (PVs) in m² and batteries capacity in Wh
    \end{itemize}
  \end{itemize}
\end{frame}



\section{Proposed solution}


\begin{frame}{Assumptions}  
  \textbf{Data centers:}
  
  \begin{itemize}

  \item IT Infrastructure already built (servers, network)
  \item Homogeneous (regarding CPU cores)
  \item Server power consumption: idle and dynamic 
  \item Intra network power consumption: static
  \item Geographically distributed
  \item Power Usage Effectiveness (PUE) for each DC

  \end{itemize}

  \begin{figure}[!h]
    \centering
    \includegraphics[width=0.6\textwidth]{images/locations_.pdf}
    \caption{Selected data centers location (inspired from Microsoft Azure)}
  \end{figure}  
\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Workload:}
  
  \begin{itemize}  
  \item All tasks must be scheduled and executed on time
  \item Task execution cannot be delayed
  \item No migration 
  \item Batch tasks that can be executed in any of the DCs 

  \end{itemize}

\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Renewable infrastructure:}
  
  \begin{itemize}    
  \item Batteries charge and discharge efficiency (85\%), Maximum
    Depth of Discharge (20\%)
  \item PVs efficiency (15\%)
  \item Carbon emissions from manufacturing PVs (250 kg \ch{CO2} eq
    per m²) and batteries (59 kg \ch{CO2} eq per kWh)
  \item Lifetime (PV: 30 years, bat: 10 years)  
  \end{itemize}
  
\end{frame}

\begin{frame}{Assumptions}  
  \textbf{Local electricity grid:}
  
  \begin{itemize}
  
  \item The energy mix is different at each location
  \item May have the presence of renewables or low carbon-intensive sources    
  
    \begin{table}
      
      \caption{Emissions (in g \ch{CO2}-eq/kWh) for using the regular grid. Source for grid emissions: electricityMap, climate-transparency.org.}\label{tab:carbonfootprint} \centering

  %    \footnotesize
      \begin{tabular}{|l|r|}
        
        \hline

        \textbf{Location} &  \textbf{Emissions }  \\
        \hline
        Johannesburg & 900.6  \\
        \hline
        Pune & 702.8\\
        \hline
        Canberra & 667.0 \\
        \hline
        Dubai & 530.0   \\
        \hline
        Singapore & 495.0  \\
        \hline     
        Seoul & 415.6  \\
        \hline
        Virginia  & 342.8  \\
        \hline
        São Paulo &  61.7 \\
        \hline 
        Paris &  52.6  \\
        \hline  

      \end{tabular}  
    \end{table}
    
  \end{itemize}
  \normalsize
\end{frame}


\begin{frame}{Proposed solution}
  \alert{Linear program (LP) formulation to minimize the cloud
    federation operation's carbon emissions} (timespan of 1 year, 1 h time slots)
  
  \begin{itemize}
    
  \item \alert{Scheduling} and \alert{dimensioning} modeled as single problem
    \begin{itemize}      
    \item Allocate workload to other DC (follow-the-renewables) or increase the battery capacity or PV area?
    \end{itemize}
  \item Only real variables 
    \begin{itemize}     
    \item  \alert{Optimal} solution in \alert{polynomial time}: 394264 variables, solved in less than \alert{1 minute} with Gurobi

    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{LP Overview - (Some) Restrictions}

  \alert{ Data center power consumption ($P^d_k $):}
  
  \begin{equation}    
    P^d_k  = PUE^d \times \big( Pintranet^d + Pidle^d  + Pcore \times w^d_k\big)
  \end{equation}

  where $PUE^d$ is the cooling efficiency at data center $d$,
  $Pintranet^d$ is the power consumption of the network devices,
  $Pidle^d$ is the server static power consumption, $PCore$ the
  dynamic power consumption of using a CPU core, and $w^d_k$ is the
  workload allocated on DC d at time slot k .
  
  \alert{Workload:}
  
  \begin{equation} \label{eq:wk}
    w_k^d \leq C^d
  \end{equation}
  
  where $C^d$ is the maximum capacity of CPU of $DC^d$ .
  
\end{frame}

\begin{frame}{LP Overview  - (Some) Restrictions}

  \alert{Batteries level of energy ($B^d_ k$):} 

  \begin{equation} \label{eq:bdk}
    B^d_k = B^d_{k-1}  + Pch^d_{k-1} \times \eta_{ch} \times \Delta{t} - \frac{Pdch^d_{k-1}}{\eta_{dch}} \times \Delta{t}
  \end{equation}

  where $Pch^d_{k-1}$ is the power charged into the batteries,
  $\eta_{ch}$ is efficiency of the charge process, $Pdch^d_{k-1}$ is
  the power discharged from the batteries and $\eta_{dch}$ is the
  efficiency of the discharge process.
  
  \alert{Renewable power production:}

  \begin{equation} \label{eq:predk}
    Pre^d_{k}= I^d_k \times Apv^d \times \eta_{pv}
  \end{equation}

  where $I^d_k$ is the solar irradiance, $Apv^d$ the PV panel area (m²),  and $\eta_{pv}$ is the efficiency of PV module

\end{frame}

\begin{frame}{LP Overview  - (Some) Restrictions}

  \alert{ Data center power supply:}

  \begin{equation} \label{eq:predk}
    P^d_k \leq Pre^d_k + Pgrid^d_k + Pdch_k^d - Pch_k^d
  \end{equation}
  
  where $Pre^d_k $ is the solar power produced at time of time slot $k$ on $DC^d$, $ Pgrid^d_k $ is the
  power used from the local electricity grid, $Pdch_k^d$ is the power
  to discharge from the battery, and $Pch_k^d$ is the power to charge
  into the battery.

\end{frame}


\begin{frame}{LP Overview}

  \alert{Objective function:}
  
  \begin{equation} \label{eq:FPALL}
    \text{minimize }\sum_{k=0}^{K-1} \sum_{d=1}^D ( FPgrid^d_k +  FPpv^d_k) + \sum_{d=1}^D FPbat^d
  \end{equation}


\end{frame}

\section{Experiments}

\begin{frame}{Sizing for one year of operation: 8760 h}
  
  \alert{ Inputs:}

  \begin{itemize}
    
  \item Solar irradiation data from 2021 (MERRA-2)
  \item Carbon emissions of using local electricity grid, manufacturing PVs and batteries
  \item Efficiency parameters of PVs and batteries
  \item Number of DCs (9), and servers (23200) and cores inside each DC
  \item Power consumption of servers and network devices (Grid'5000 Taurus, HP ProCurve 2810-48G)
  \item PUE of each DC
  \item Workload (based on Google traces)

  \end{itemize}

  \alert{ Ouptut}

  \begin{itemize}
  \item Area of PVs (m²) and capacity of batteries (Wh)
   \item Total carbon emissions
  \end{itemize}

\end{frame}


\section{Results}


\begin{frame}{Results}
  \begin{figure}[!htbp]
    \centering
    \includegraphics[width=1\textwidth]{images/sizing.pdf}
    \caption{Optimal result for the renewable sizing.}
    \label{fig:sizing}
  \end{figure}

\end{frame}




\begin{frame}{Results}
  \begin{figure}[!htbp]
    \centering
    \includegraphics[width=.6\textwidth]{images/energy_ratio.pdf}
    \caption{Composition of the DCs’ daily energy consumption throughout the
      year considering the different sources of energy}
    \label{fig:sizing}
  \end{figure}
\end{frame}


\begin{frame}{Results}


  \begin{table}[!ht]
    \caption{Total emissions for the different scenarios.}\label{tab:emissions} \centering
    \begin{tabular}{|p{5cm}|r|}
      \hline
      \textbf{Scenarios} & \textbf{Emissions (t \ch{CO2}-eq)}   \\
      \hline
      Electrical grid                    & 201211.3    \\
      \hline
      PV and batteries        &                42370.6 \\ 
      \hline
      PV, batteries, and grid             &  29600.6   \\
      \hline
    \end{tabular}
  \end{table}
  Reductions on carbon emissions:
   \begin{itemize}
  \item Local grid vs DC renewables infra: \alert{$\simeq 5$ times}
   \item Local grid only vs DC renewables infra and local grid: \alert{$\simeq 6$ times}
  \end{itemize}
\end{frame}



\begin{frame}{Results}
  
\begin{table}[!ht]
  
  \caption{Evaluating sizing considering solar irradiation data of different years (2018, 2019, 2020) using the MAPE metric (values are in \%) }\label{tab:years_MAPE} \centering

  \begin{tabular}{|l|r|r|}
   \hline
    
  \textbf{Location} &   \textbf{PV Area} & \textbf{Battery Capacity} \\
  \hline
  Johannesburg & 1.72 & 1.64  \\
  \hline
  Pune  & 3.72 & 0.76  \\
  \hline
  Canberra  & 8.62 & 4.25 \\
  \hline
  Dubai   & 2.31 & 2.88   \\
  \hline
  Singapore & 7.22 & 0.34 \\
  \hline     
  Seoul    & 3.15 & 1.11 \\
  \hline
  Virginia   & 2.2 & 0.87 \\
  \hline
  São Paulo   & 5.81 & 8.05 \\
  \hline 
  Paris    & 2.76 & 0     \\
    \hline
    
  \end{tabular}  
\end{table}

\end{frame}


\begin{frame}{Visualization of DC operation and energy source used}
  
  \begin{itemize}    
  \item Each circle is a DC, and the radius is the power consumption (MW)
  \item The pizza graph represents the share of electricity source being used at that instant (PVs,batteries, or from the grid)
  \item The gray shadow represents the night
  \item Visualization for the first week of 2021
  \end{itemize}

  \begin{center}
    \begin{figure}[!h]
      \centering
      \includegraphics[width=1\textwidth]{images/dc_op_plot.png}
      \caption{Example of data centers electricity source used.}
    \end{figure}
  \end{center}
\end{frame}

\begin{frame}{Results}
  \begin{center}
    \movie[width=8.1cm, height =8.1cm]{Example of DCs electricity source used.}{dc_op.mp4}
  \end{center}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion}
  \begin{itemize}    
  \item \alert{Linear program to reduce the carbon footprint of operating a cloud federation}
    \begin{itemize}
    \item Scheduling and sizing
    \item Emissions from manufacturing the renewable infrastructure
    \item Grid emissions, solar irradiation, and cooling needs (PUE) of each DC's location
    \end{itemize}
    
  \item \alert{Proposed solution uses only linear variables}
    \begin{itemize}
    \item Optimally solve large scenarios in polynomial time
    \end{itemize}
      
  \item \alert{Optimal solution specific per location}
    \begin{itemize}
    \item Hybrid configuration: grid and renewables
    \item Batteries are not always mandatory
    \end{itemize}
  
  \item \alert{Flexibility of the model}

    \begin{itemize}
    \item Evaluate other scenarios: more DCs, other servers, locations for DCs (grid emissions and solar irradiation data), workload
    \end{itemize}
    
  \end{itemize}
\end{frame}



\begin{frame}{Future work}
  \begin{itemize}
  \item Robustness of the model
    \begin{itemize}
    \item Climate conditions and workload
    \end{itemize}
  \item Consider task migration and other scheduling algorithms
    \begin{itemize}
    \item Quality of Service vs carbon emissions
    \end{itemize}
  \item Other workload types (not only batch tasks)
  \item Dimensioning of IT infrastructure
    \begin{itemize}
    \item new servers' energy efficiency vs manufacturing emissions
    \end{itemize}
    

  \end{itemize}

\end{frame}

\begin{frame}{Sponsor acknowledgments}

This work has been partially supported by the LabEx PERSYVAL-Lab (``ANR-11-LABX-0025-01'') funded by the French program \textit{Investissement d'avenir}, by grant \#2021/06867-2, São Paulo Research Foundation (FAPESP), by the EIPHI Graduate school (contract ``ANR-17-EURE-0002''), by the EuroHPC EU Regale project (g.a. 956560), and by the ANR DATAZERO2 (contract ``ANR-19-CE25-0016'') project.
\end{frame}

\begin{frame}{Thank you!}

  \textbf{Optimal sizing of a globally distributed low carbon cloud federation}

  \begin{center}
    \begin{figure}
      \centering
      \includegraphics[width=0.4\textwidth]{images/qr-code.png}

      \caption{Reproducible artifact: https://doi.org/10.25666/dataubfc-2023-02-03  }
    \end{figure}
    
  
    \textbf{Contact:}
    
    \alert{miguel.silva-vasconcelos@inria.fr}

  \end{center}
\end{frame}


\begin{frame}[allowframebreaks]  
  \printbibliography
\end{frame}


\begin{frame}
\begin{table}
  
  \caption{Emissions (in g \ch{CO2}-eq/kWh) for both PV usage and using the regular grid. Source for grid emissions: electricityMap, climate-transparency.org.}\label{tab:carbonfootprint} \centering

  \begin{tabular}{|l|r|r|}
    
  \hline

  \textbf{Location} &  \textbf{Grid} & \textbf{PV} \\
  \hline
  Johannesburg & 900.6 & 24.90 \\
  \hline
  Pune & 702.8 & 27.96 \\
  \hline
  Canberra & 667.0 & 29.71 \\
  \hline
  Dubai & 530.0  & 24.84 \\
  \hline
  Singapore & 495.0 & 36.19 \\
  \hline     
  Seoul & 415.6 & 34.00 \\
  \hline
  Virginia  & 342.8 & 31.71 \\
  \hline
  São Paulo &  61.7 & 27.99\\
  \hline 
  Paris &  52.6  & 39.93 \\
  \hline
    
\end{tabular}  
\end{table}


 \begin{equation} \label{eq:pvco2}
   pvCO2^d =  \frac{FPpv_{1m2}}{expectedEpv^d} 
\end{equation}



\end{frame}





\begin{frame}

  
\begin{table}[!ht]
  
  \caption{Results of the sustainability metrics for the experiments}\label{tab:metrics} \centering

  \begin{tabular}{|l|r|r|}    
  \hline

  \textbf{Location} &  \textbf{GEC} & \textbf{\ch{CO2} savings (\%)} \\
  \hline
  Johannesburg & 1.47 & 93.93 \\
  \hline
  Pune & 1.45 & 91.5 \\
  \hline
  Canberra & 1.57 & 89.59 \\
  \hline
  Dubai & 1.59  & 89.1 \\
  \hline
  Singapore & 1.42 & 85.75 \\
  \hline     
  Seoul & 1.53 & 82.51 \\
  \hline
  Virginia  & 1.46 & 75.99 \\
  \hline
  São Paulo &  0.5 & 20.05 \\
  \hline 
  Paris &  0.24  & 5.25 \\
    \hline
   
\end{tabular}  

\end{table}


\begin{equation} \label{eq:co2savings}
  CO2_{savings} = \left( 1 -  \frac{CO2_{current}} {CO2_{baseline}} \right) \times 100 
\end {equation}



\end{frame}


\begin{frame}

\begin{table}[!ht]
  
  \caption{Average DC load throughout the year }\label{tab:dcutilization} \centering

  \begin{tabular}{|l|r|r|r|}
   \hline
    
  \textbf{Location} &   \textbf{Grid} & \textbf{PV + Bat} & \textbf{PV + Bat + Grid}  \\
  \hline
  Johannesburg & 0 & 79.31  & 86.20  \\
  \hline
  Pune  & 10.25 &  82.07 & 89.34   \\
  \hline
  Canberra  & 99.72 & 66.62 & 67.95 \\
  \hline
  Dubai   & 99.97 & 93.93 & 95.11   \\
  \hline
  Singapore & 99.93 & 72.6  & 85.18 \\
  \hline     
  Seoul    & 99.99 & 81.87 & 65.39      \\
  \hline
  Virginia   & 100.0 & 88.54 & 75.51 \\
  \hline
  São Paulo   & 100.0 & 63.67 & 59.06 \\
  \hline 
  Paris    & 100.0 & 81.24  &  86.11    \\
  \hline  

\end{tabular}  

\end{table}



\begin{equation}\label{eq:dcload}
 \frac{\sum_k w^d_k} {C^d \times K }
\end{equation}

Equation~\eqref{eq:dcload} shows how the DC load metric is computed,
where $K$ is the total number of time $k$ slots


\end{frame}


\begin{frame}{Results}

  \begin{itemize}
    \item  Facebook's largest data center area: \alert{ $\simeq
        420000$  m² }  \footnote{\url{https://dgtlinfra.com/facebook-18-data-centers-20bn-investment/}}
    \item Project of one of largest grid utility batteries: 2.4 GWh and \alert{ $\simeq$ 89000 m²} \footnote{\url{https://www.morrobayca.gov/DocumentCenter/View/15093/Vistra---Morro-Bay---Battery-Project-Presentation-022021}}
  \end{itemize}
  
  \begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.6\textwidth]{images/sizing.pdf}
    \caption{Optimal result for the renewable sizing.}
    \label{fig:sizing}
  \end{figure}
  
\end{frame}






\begin{frame}{Results}


\begin{table}[H]
\small

  \caption{Comparison of local electricity grid access to information }\label{tab:grid_emissions_hist} \centering

  \begin{tabular}{|l|r|r|r|}
   \hline
    
  \textbf{Location} &   \textbf{Granularity} & \textbf{Span} & \textbf{Source} \\
  \hline
  Johannesburg & month & year & ElectricityMaps  \\
  \hline
  Pune  & hour & year & ElectricityMaps  \\
  \hline
  Canberra  & hour &  year & ElectricityMaps \\
  \hline
  Dubai     & year & year & 1p5ndc-pathways.climateanalytics.org  \\
  \hline
  Singapore & month & year & ElectricityMaps \\
  \hline     
  Seoul     & hour & year & ElectricityMaps \\
  \hline
  Virginia  &  hour & year & ElectricityMaps \\
  \hline
  São Paulo & hour & year  & ElectricityMaps \\
  \hline 
  Paris     & hour & year  & ElectricityMaps  \\
  
  \hline  
\end{tabular}  
\end{table}



\begin{table}[H]

\small
  \caption{Total emissions (tons of \ch{CO2}) for different scenarios }\label{tab:co2_grid_granularities} \centering
  
  \begin{tabular}{|l|r|}
   \hline
    
  \textbf{Scenario} &   \textbf{Total \ch{CO2} emissions (tons)} \\
  \hline
  Fine grain grid emissions data & 30911.03 \\
  \hline
  Average of the year grid emissions data & 30831.14 \\
  \hline

\end{tabular}  
\end{table}

  
\end{frame}




\end{document}



